from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
from time import sleep
import sys

chrome_path = "/home/galex/Desktop/chromedriver"

driver = webdriver.Chrome(chrome_path)

f = open('propLinks1.txt', 'r')
line = f.readline()

while line:
    if len(line) > 5:
        driver.get("http://www.ricacorp.com" + line)
        sleep(1)
        soup = BeautifulSoup(driver.page_source, "lxml")
        try:
            agentName = soup.findAll("a", {"class": "name txt15_black"})[0].text.split()
            agentMail = soup.findAll("a", {"class": "email txt12_black cboxElement"})[0].text
            agentPhone = soup.findAll("strong", {"class": "mobile txt15_blue"})[0].text
            print agentName[1] + " " + agentName[2] + ", " + agentMail + ", " + agentPhone
            line = f.readline()
        except:
            sleep(1)
            line = f.readline()
    else:
        line = f.readline()
