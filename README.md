# Scraping Javascript content with Selenium and Beautiful Soup
This is a side project I did for 1PLACE HK to experiment scraping JavaScript rendered content with Chrome Driver, Selenium and Beautiful Soup.

The goal of this project was to scrape the contact information of all real estate agents who have listings on the site.
I took the following steps to complete the task:
1. Scrape the property ids on the home page, store them in a textfile
2. Read the textfile, scrape the agents information through accessing individual pages of listings
3. Remove all the duplicated agents

It is very interesting to see how Selenium actions can assist in scraping js pages continuously. 
I had a lot of fun trying this out!

### Installation
Install Chrome Webdriver, Python, Beautiful Soup and Selenium. The url can be found below:

https://sites.google.com/a/chromium.org/chromedriver/downloads
https://www.crummy.com/software/BeautifulSoup/bs4/doc/
http://www.seleniumhq.org/

Place chrome driver on your Desktop

Go to the directory of the app, 
``` bash
python propertyScrape.py  >> propLinks1.txt 
```
This will scrape all the properties ID.

Now, we will scrape the agents info
``` bash
python agentScrape.py >> allAgents.txt
```

To get all the unique property agents, simply run the shell script
``` bash
sort allAgents.txt | uniq >> uniqueAgents.csv
```
PRAISE THE SUN!!!

PS. Thanks for checking my repo out!!!
