from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from bs4 import BeautifulSoup
from time import sleep
import sys

url = "http://www.ricacorp.com/rcproperty/search/d~s~n~sd~l~1/na~na~na~na~na~na~na~na~na~na"
totalPages = 1090
page = 0

chrome_path = "/home/galex/Desktop/chromedriver"
driver = webdriver.Chrome(chrome_path)
driver.get(url)

soup = BeautifulSoup(driver.page_source, "lxml")

while page < totalPages:
    # Get all the property links and print it out
    postLinks = soup.findAll('a', {'class': 'postLink'})
    postLinks.pop()
    postLinks.pop()
    print(page)
    for link in postLinks:
        print link['href']

    while True:
        try:
            driver.find_element_by_xpath('//*[@id="searchResultLowerPager"]/span/a[2]').click()
            break
        except:
            sleep(1)
    page += 1
    soup = BeautifulSoup(driver.page_source, "lxml")
